# Personal Site
Code for the site is under the MIT license. Aside from the brand icons (GitHub, itch.io, Mastodon, Twitter) and the artworks of A World of Order and One Last Wish, all assets are free for you to use (CC-BY) :).
